package com.example.jobsity.justjava;

import android.content.Intent;
import android.net.Uri;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.attr.name;

public class MainActivity extends AppCompatActivity {
    int quantity = 2;
    boolean hasWhippedCream = false;
    boolean hasChocolate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void incrementQuantity(View view){
        if(quantity == 100){
            Toast.makeText(this,"The quantity should by less than one", Toast.LENGTH_SHORT).show();
            return;
        }
        quantity++;
        displayQuantity(quantity);
    }

    public void decrementQuantity(View view){
        if(quantity == 1){
            Toast.makeText(this,"The quantity should by more than one hundred ", Toast.LENGTH_SHORT).show();
            return;
        }
        quantity--;
        displayQuantity(quantity);
    }

    private void displayQuantity(int quantity){
        TextView quantityText = (TextView) findViewById(R.id.quantity_text);
        quantityText.setText(Integer.toString(quantity));
    }

    public void submitOrder(View view){
        CheckBox whippedCreamCheckBox = (CheckBox) findViewById(R.id.whipped_cream_check);
        CheckBox chocolateCheckBox = (CheckBox) findViewById(R.id.chocolate_check);
        EditText nameEditText = (EditText) findViewById(R.id.name_edit);

        hasWhippedCream = whippedCreamCheckBox.isChecked();
        hasChocolate = chocolateCheckBox.isChecked();
        float price = calculatePrice(hasWhippedCream, hasChocolate);

        String name = nameEditText.getText().toString();
        String orderSummary = createOrderSummary(price, hasWhippedCream, hasChocolate, name);

       composeEmail("Just Java order for " + name, orderSummary);
    }

    public void composeEmail(String subject, String emailBody) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, emailBody);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private float calculatePrice(boolean hasWhippedCream, boolean hasChocolate){
        int basePrice = 5;

        if(hasWhippedCream){
            basePrice += 1;
        }

        if(hasChocolate){
            basePrice += 2;
        }

        return quantity * basePrice;
    }

    private String createOrderSummary(float price, boolean hasWhippedCream, boolean hasChocolate, String name){
        String priceMessage = getString(R.string.order_summary_name, name);
        priceMessage += "\nhas whipped cream: " + hasWhippedCream;
        priceMessage += "\nhas chocolate: " + hasChocolate;
        priceMessage += "\nQuantity: " + quantity;
        priceMessage += "\nPrice: $" + price;
        priceMessage += "\n" + getString(R.string.thank_you);
        return priceMessage;
    }
}
